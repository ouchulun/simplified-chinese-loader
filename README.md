## simplified-chinese-loader

优化精简编码逻辑

一个繁体转换简体的工具webpack-loader ，请在loader中直接使用

使用方法

vue

```
{
    test: /\.(js|vue)$/,
    loader: 'simplified-chinese-loader',
}
```


react

```
{
    test: /\.(js|.jsx)$/,
    loader: 'simplified-chinese-loader',
}
```

更新包步骤：
npm login (chulun)
npm publish